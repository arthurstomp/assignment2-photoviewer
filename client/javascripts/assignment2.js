(function () {
"use strict";

var helpState = true;
var pauseState = false;
var playSound = true;
var showPuzzle = true;
var rows = 2;
var cols = 2;
var cells = 4;
var id = "u_ErxlI03V4";
var proxy = "http://server7.tezzt.nl:1332/api/proxy";

var TURN_ON_KEY_CODE = 49;//Corresponds to 1 key;
var TURN_OFF_KEY_CODE = 48;//Corresponds to 0 key;

var LEFT_ARROW = 37;
var UP_ARROW = 38;
var RIGHT_ARROW = 39;
var DOWN_ARROW = 40;

var TOGGLE_HELP_KEY = 72;
var PAUSE_KEY = 80;

var VIDEO_HEIGHT = 360;
var VIDEO_WIDTH = 640;

window.KeyboardHandler = {
handleSound : function(soundIsOn){
if(soundIsOn){
  playSound = true;
  Puzzle.video.volume = 1;
}else{
  playSound = false;
  Puzzle.video.volume = 0;
}
},

handleMovement: function(moveDirection){
switch(moveDirection){
  case LEFT_ARROW:
    console.log("move to the left ->");
    Puzzle.moveBlankTileLeft();
    break;
  case UP_ARROW:
    Puzzle.moveBlankTileUp();
    break;
  case RIGHT_ARROW:
    Puzzle.moveBlankTileRight();
    break;
  case DOWN_ARROW:
    Puzzle.moveBlankTileDown();
    break;
  default:
}
return false;
},
handleHelp: function(){
helpState = !helpState;
},

handlePause: function(){
pauseState = !pauseState;
if(pauseState){
  Puzzle.video.pause();
}else{
  Puzzle.video.play();
}
},

handleKeydown: function(keyEvent){
var keyCode = keyEvent.keyCode;
//Handle sound
if(keyCode == TURN_OFF_KEY_CODE || keyCode == TURN_ON_KEY_CODE){
  if(keyCode == TURN_OFF_KEY_CODE){
    KeyboardHandler.handleSound(false);
  }else if(keyCode == TURN_ON_KEY_CODE){
    KeyboardHandler.handleSound(true);
  }
}

//Handle movement
if(keyCode == LEFT_ARROW ||
    keyCode == UP_ARROW ||
    keyCode == RIGHT_ARROW ||
    keyCode == DOWN_ARROW
  )
{
  return KeyboardHandler.handleMovement(keyCode);
}

//Handle help
if(keyCode == TOGGLE_HELP_KEY){
  KeyboardHandler.handleHelp();
}

//handle pause
if(keyCode == PAUSE_KEY){
  KeyboardHandler.handlePause();
}
},
};


window.PuzzleForm = {
youtubeLink : null,
nRows : null,
nCols : null,
createPuzzleSubmit : null,

init : function(){
PuzzleForm.youtubeLink = document.getElementById("youtubeLink");
PuzzleForm.nRows = document.getElementById("rows");
PuzzleForm.nCols = document.getElementById("columns");
PuzzleForm.createPuzzleSubmit = document.getElementById("createPuzzleSubmit");
PuzzleForm.settingEventHandlers();
},

youtubeLinkBlur : function(){
PuzzleForm.getYoutubeLinkId(PuzzleForm.youtubeLink.value);
},

getYoutubeLinkId : function(link){
var splittedLink = PuzzleForm.youtubeLink.value.split("=");
id = splittedLink[1];
},

createPuzzleSubmitClick : function(){
Puzzle.createPuzzle();
},
nRowsChange : function(){
rows = PuzzleForm.nRows.value;
cells = rows * cols;
},
nColsChange : function(){
cols = PuzzleForm.nCols.value;
cells = rows * cols;
},
settingEventHandlers : function(){
PuzzleForm.youtubeLink.addEventListener("blur",PuzzleForm.youtubeLinkBlur);
PuzzleForm.nRows.addEventListener("change",PuzzleForm.nRowsChange);
PuzzleForm.nCols.addEventListener("change",PuzzleForm.nColsChange);
PuzzleForm.createPuzzleSubmit.addEventListener("click",PuzzleForm.createPuzzleSubmitClick);
},
};

window.Puzzle = {
tiles : [cells],
blankTile : null,
playerWon : false,
video : null,

destroyPuzzle : function(puzzle){
var puzzleChildren = puzzle.children;
while(puzzle.firstChild){
  puzzle.removeChild(puzzle.firstChild);
}
},

relativePositionBlankTilePositionFromTile : function(tile){
var blankTileCurrentPosition = Puzzle.blankTile.currentPosition;
var tileCurrentPosition = tile.currentPosition;

//Horizontal movement
if(blankTileCurrentPosition == tileCurrentPosition + 1 && (blankTileCurrentPosition % cols > tileCurrentPosition % cols)){
  Puzzle.moveBlankTile(tile);
}else if(blankTileCurrentPosition == tileCurrentPosition - 1 && (blankTileCurrentPosition % cols < tileCurrentPosition % cols) ){
  Puzzle.moveBlankTile(tile);
}
//Vertical movement
if(blankTileCurrentPosition < tileCurrentPosition && blankTileCurrentPosition % cols == tileCurrentPosition % cols && Math.abs(blankTileCurrentPosition - tileCurrentPosition) <= cols ){
  Puzzle.moveBlankTile(tile);
}else if(blankTileCurrentPosition > tileCurrentPosition && blankTileCurrentPosition % cols == tileCurrentPosition % cols && Math.abs(blankTileCurrentPosition - tileCurrentPosition) <= cols ){
  Puzzle.moveBlankTile(tile);
}
},

tileClick : function(e){
var tile = e.toElement;
var tileId = tile.id.split("_")[1];
var blankTile = document.getElementById("blankTile");
Puzzle.relativePositionBlankTilePositionFromTile(tile);
},

shuffleTiles : function(){
for(var i = 0 ; i < Puzzle.tiles.length ; i ++){
  var j =  Math.floor(Math.random() * 10) % Puzzle.tiles.length;
  var auxTile = Puzzle.tiles[i];
  Puzzle.tiles[j].currentPosition = i;
  Puzzle.tiles[i] = Puzzle.tiles[j];
  auxTile.currentPosition = j;
  Puzzle.tiles[j] = auxTile;
}
return Puzzle.tiles;
},

placeTiles : function(){
for(var j = 0 ; j < cells  ; j++){
  var tile = Puzzle.tiles[j];
  document.getElementById("puzzle").appendChild(tile);
  if((j+1) % cols === 0){
    var c = document.createElement("div");
    c.className = "clear" ;
    document.getElementById("puzzle").appendChild(c);
  }
}
},

createPuzzle : function(){
var puzzle = document.getElementById("puzzle");
var tileHeight = VIDEO_HEIGHT / rows;
var tileWidth = VIDEO_WIDTH / cols;

Puzzle.destroyPuzzle(puzzle);

Puzzle.startVideo();

},

removeTiles : function(){
while(document.getElementById('puzzle').firstChild){
  document.getElementById('puzzle').removeChild(document.getElementById('puzzle').firstChild);
}
},

swapTiles : function(blankTile, tile){
var iBT = Puzzle.tiles.indexOf(blankTile);
var iT = Puzzle.tiles.indexOf(tile);
var auxBT = blankTile;

tile.currentPosition = iBT;
Puzzle.tiles[iBT] = tile;
auxBT.currentPosition = iT;
Puzzle.tiles[iT] = auxBT;

Puzzle.removeTiles();
Puzzle.placeTiles();
},

checkWin : function(){
var ok = true;
for(var i = 0; i < Puzzle.tiles.length ; i++){
  var tile = Puzzle.tiles[i];
  if(tile.currentPosition != tile.originalPosition && tile.id !='blankTile'){
     ok = false;
  }
}
if(ok){
  Puzzle.playerWon = true;
  alert("you won!");
}
return ok;
},

moveBlankTile : function(tile){
Puzzle.swapTiles(Puzzle.blankTile,tile);
Puzzle.checkWin();
},

moveBlankTileLeft : function(){
console.log("call move left");
var iBT = Puzzle.tiles.indexOf(Puzzle.blankTile);
console.log("iBT = "+iBT);
if(iBT-1 >= 0){
  var leftTile = Puzzle.tiles[iBT - 1];

  console.log(leftTile.currentPosition / cols);
  console.log(Puzzle.blankTile.currentPosition / cols);
  if(Math.floor(leftTile.currentPosition / cols) == Math.floor(Puzzle.blankTile.currentPosition / cols)){
    console.log("move left");
    Puzzle.moveBlankTile(leftTile);
  }
}
},

moveBlankTileUp : function(){
console.log("call move up");

if(Puzzle.blankTile.currentPosition > cols - 1){
  console.log("blank tile can go up");
  var upTile = Puzzle.tiles[Puzzle.blankTile.currentPosition - cols];
  Puzzle.moveBlankTile(upTile);
}

},

moveBlankTileRight : function(){
console.log("call move right");
var iBT = Puzzle.tiles.indexOf(Puzzle.blankTile);
console.log("iBT = "+iBT);
if(iBT+1 < Puzzle.tiles.length){
  var leftTile = Puzzle.tiles[iBT + 1];

  console.log(leftTile.currentPosition / cols);
  console.log(Puzzle.blankTile.currentPosition / cols);
  if(Math.floor(leftTile.currentPosition / cols) == Math.floor(Puzzle.blankTile.currentPosition / cols)){
    console.log("move right");
    Puzzle.moveBlankTile(leftTile);
  }
}
},

moveBlankTileDown : function(){
console.log("call move down");

console.log(Puzzle.blankTile.currentPosition);
console.log(cols);
console.log(Puzzle.tiles);
if(Puzzle.blankTile.currentPosition <= (cols * rows) - 1 - cols){
  console.log('blank tile can go down');
  console.log(parseInt(Puzzle.blankTile.currentPosition) + parseInt(cols));
  var downTile = Puzzle.tiles[parseInt(Puzzle.blankTile.currentPosition) + parseInt(cols)];
  console.log(downTile);
  Puzzle.moveBlankTile(downTile);
}
},

createTiles : function(){
for(var i = 0 ; i < cells ; i ++){
  var tile = document.createElement("canvas");
  tile.height = Puzzle.video.height/rows;
  tile.width = Puzzle.video.width/cols;
  tile.currentPosition = i;
  tile.originalPosition = i;
  tile.className = "tile";
  if(i != cells - 1){
    tile.id = "tile_"+i;
    tile.style.backgroundColor = "red";
    tile.title = i;
    tile.addEventListener("click",Puzzle.tileClick);
  }else{
    tile.id = "blankTile";
    tile.src = "./images/1x1.png";
    Puzzle.blankTile = tile;
  }

  Puzzle.tiles[i] = tile;
}
},

drawTiles : function(){
 var tileWidth = Puzzle.video.width/cols;
 var tileHeight = Puzzle.video.height/rows;
 var heightMult = 0;
 var inc = 0;
 for(var i = 0; i < Puzzle.tiles.length ; i++){
   var canvas = Puzzle.tiles[i];
   var originalPosition = canvas.originalPosition;
   var widthMult = originalPosition % cols;
   var context = canvas.getContext('2d');
   heightMult = Math.floor(originalPosition / cols);
   var x = Math.floor(widthMult * tileWidth);
   var y = Math.floor(heightMult * tileHeight);
   if(canvas.id != "blankTile"){
     context.drawImage(Puzzle.video, x, y, tileWidth, tileHeight, 0, 0, tileWidth, tileHeight);
   }
   setTimeout(Puzzle.drawTiles,5);
   inc = inc + 1;
 }
},

playVideo : function(){
Puzzle.createTiles();
Puzzle.drawTiles();
Puzzle.tiles = Puzzle.shuffleTiles();
Puzzle.placeTiles();
},

startVideo : function(){
  var videoElement = document.createElement("video");
  videoElement.id = "puzzle_video";
  videoElement.width = VIDEO_WIDTH;
  videoElement.height = VIDEO_HEIGHT;
  videoElement.autoplay = "autoplay";
  videoElement.loop = true;
  console.log("Play sound = "+playSound);
  if(playSound){
    videoElement.volume = 1;
  }else{
    videoElement.volume = 0;
  }
  videoElement.playbackRate = 1;

  var vid = new YoutubeVideo(id,proxy,function(video){
    var source = video.getSource("video/webm", "medium");
    videoElement.src = source.url;
    Puzzle.video = videoElement;
    Puzzle.video.addEventListener("play",Puzzle.playVideo);
  });

},
};

document.addEventListener("DOMContentLoaded",function(){
document.addEventListener("keydown",KeyboardHandler.handleKeydown);

window.PuzzleForm.init();
});
})();
